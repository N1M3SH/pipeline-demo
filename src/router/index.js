import { createRouter, createWebHistory } from 'vue-router'
import Documentation from '@/components/Documentation.vue'
import Code from '@/components/Code.vue'


const routes = [
    {
        path:'/',
        name:'document',
        component: Documentation
    },
    {
        path:'/pipe-line-settings',
        name:'ymlFile',
        component: Code
    }
]


const router = createRouter({
    history: createWebHistory(),
    routes
})
  
export default router;